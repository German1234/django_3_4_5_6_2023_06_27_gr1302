# django_3_4_5_6_2023_06_27_gr1302

---

[video 1](https://youtu.be/JxH7SxNbUNg) - models and migrations

[video 2](https://youtu.be/Oovjju2Fkas) - views

[video 3](https://youtu.be/qZ4AmOJjAjU) - templates

[vifdeo 4](https://youtu.be/cTl23TO14Wg) - forms

[video 5](https://youtu.be/3P_6XePQi0M) - generic views, begin for drf

[video 6](https://youtu.be/TPoNoiuHu44) - auth

[video 7](https://youtu.be/yFTG-q_fQJM) - auth 2

[video 8](https://youtu.be/2uVoecZtl6c) - auth 3, first day of project

[video 9](https://youtu.be/7cUO9QPA8vY) - social auth

[video 10](https://youtu.be/qZXmiH1varM) - project 3, deploy


