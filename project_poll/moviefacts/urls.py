from django.urls import path
from moviefacts import views


urlpatterns = [
    path("citizen_kane/", views.citizen_kane),
    path("casablanca/", views.casablanca),
    path("maltese_falcon/", views.maltese_falcon),
    path("psycho/", views.psycho),
    path("listing/", views.listing)
]