from django.shortcuts import render
from django.http import HttpResponse
from django.template import Context, Template


def citizen_kane(request):
    content = """{{movie}} was realeased in {{year}}"""
    template = Template(content)
    context = Context({"movie": "Citizen Kane", "year": 1941})

    result = template.render(context)
    return HttpResponse(result)


def casablanca(request):
    return render(
        request, "moviefacts/simple.txt", {"movie": "Casablanca", "year": 1942}
    )


def maltese_falcon(request):
    return render(
        request, "moviefacts/falcon.html", {"movie": "Maltese Falcon", "year": 1941}
    )


def psycho(request):
    data = {
        "movie": "Psycho",
        "year": 1960,
        "is_scary": True,
        "color": False,
        "tomato_meter": 96,
        "tomato_audience": 95,
    }
    return render(request, "moviefacts/psycho.html", data)


def listing(request):
    data = {
        "movies": [
            (
                "Citizen Kane",
                1941
            ),
            (
                "Casablanca",
                1942,
            ),
            (
                "Maltese Falcon",
                1942,
            ),
            (
                "Psycho",
                1960,
            )
        ]
    }
    return render(request, "moviefacts/listing.html", data)