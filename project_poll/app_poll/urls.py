from django.urls import path

from . import views, views_gen

app_name = "app_poll"
urlpatterns = [
    # path("", views.index, name="index"),
    # path("<int:question_id>/", views.detail, name="detail"),
    # path("<int:question_id>/results/", views.results, name="results"),
    path("<int:question_id>/vote/", views.vote, name="vote"),
    path("gen/", views_gen.IndexView.as_view(), name="index"),
    path("gen/<int:pk>/", views_gen.DetailView.as_view(), name="detail"),
    path("gen/<int:pk>/results/", views_gen.ResultView.as_view(), name="results"),
]
