from django.shortcuts import render, redirect, reverse
from django.contrib.auth.views import LoginView, PasswordChangeView, LogoutView, PasswordChangeDoneView, PasswordResetView, PasswordResetDoneView, PasswordResetCompleteView, PasswordResetConfirmView
from django.urls import reverse_lazy
from django.http import HttpRequest, HttpResponse
from users.forms import CustomerUserCreationForm
from django.contrib.auth import login
# from django.contrib.auth.urls import urlpatterns


def dashboard(request):
    return render(request, "users/dashboard.html")


def register(request: HttpRequest):
    if request.method == "GET":
        return render(
            request,
            "users/register.html",
            {"form": CustomerUserCreationForm}
        )
    elif request.method == "POST":
        form = CustomerUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect(reverse("users:dashboard"))


class MyLoginView(LoginView):
    template_name = "registration/login.html"


# class MyLogoutView(LogoutView):
#     template_name = "users/registration/login.html"


class MyPasswordChangeView(PasswordChangeView):
    template_name = "registration/my_password_change_form.html"
    success_url = reverse_lazy("users:password_change_done")


class MyPasswordChangeDoneView(PasswordChangeDoneView):
    template_name = "registration/my_password_change_done.html"


class MyPasswordResetView(PasswordResetView):
    template_name = "registration/my_password_reset_form.html"
    success_url = reverse_lazy("users:password_reset_done")
    email_template_name = "registration/my_password_reset_email.html"


class MyPasswordResetDoneView(PasswordResetDoneView):
    template_name = "registration/my_password_reset_done.html"


class MyPasswordResetConfirmView(PasswordResetConfirmView):
    success_url = reverse_lazy("users:my_password_reset_complete")
    template_name = "registration/my_password_reset_confirm.html"


class MyPasswordResetCompleteView(PasswordResetCompleteView):
    template_name = "registration/my_password_reset_complete.html"
