from django.urls import path, include
from users import views


app_name = "users"
urlpatterns = [
    path("auth/dashboard/", views.dashboard, name="dashboard"),
    path("auth/accounts/login/", views.MyLoginView.as_view(), name="login"),
    path("auth/accounts/password_change/", views.MyPasswordChangeView.as_view(), name="password_change"),
    path("auth/accounts/password_change/done/", views.MyPasswordChangeDoneView.as_view(), name="password_change_done"),
    path("auth/accounts/password_reset/", views.MyPasswordResetView.as_view(), name="password_reset"),
    path("auth/accounts/password_reset/done/", views.MyPasswordChangeDoneView.as_view(), name="password_reset_done"),
    path("auth/accounts/reset/<uidb64>/<token>/", views.MyPasswordResetConfirmView.as_view(), name="password_reset_confirm"),
    path("auth/accounts/reset/done/", views.MyPasswordResetCompleteView.as_view(), name="password_reset_complete"),
    path("auth/accounts/", include("django.contrib.auth.urls")),
    path("auth/register/", views.register, name="register" ),
]